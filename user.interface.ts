
export interface IAuth {
    auth: IAuthResponse
}

export interface IAuthResponse {
    token: string;
}

export interface IAuthPayload {
    login: string,
    password: string,
}